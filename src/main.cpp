#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <ilcplex/ilocplex.h>
#include "Usage.h"

using std::cerr;
using std::cout;
using std::endl;



int main(int argc, char *argv[]) {
	if (argc <= 1)
		usage(argv[0]);

	/* Loop over all command line arguments. */
	for (int a = 1; a < argc; ++a) {

		std::cout << "\n\033[32m====================================="
				<< "====================================\033[37m\n";
		std::cout << "\t>>> Start Sequentiel cplex  for Instance ";
		std::cout << argv[a] << "\n";
		std::cout << "\033[32m====================================="
				<< "=====================================\033[37m\n";
			try {

				IloEnv env;
				IloModel model(env);
				IloCplex cplex(model);
				IloObjective obj(env);
				IloNumVarArray x(env);
				IloRangeArray cons(env);

				cplex.setParam(IloCplex::Param::Threads, 1); //number of threads control 

				cplex.importModel(model, argv[a], obj, x, cons); // read instance file

				if (cplex.solve()) {
					// solve the problem
				}

				env.end();

			} catch (IloException& e) {
				std::cerr << "Concert Exception: " << e << endl;
			}
		std::cout << "\n\033[32m====================================="
				<< "====================================\033[37m\n";
		std::cout << "\t>>> End for Instance " << argv[a] << "\n";
		std::cout << "\033[32m====================================="
				<< "=====================================\033[37m\n";
	}

	return 0;
}
