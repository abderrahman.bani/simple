

#pragma once
#include <iostream>
#include <stdlib.h>

// Print a usage message and exit.
static void usage(const char *progname) {
	std::cerr << "Usage: " << progname << " instance-name..." << std::endl;
	exit(2);
}