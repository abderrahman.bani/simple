#===============================================
# makefile with cplex 
#===============================================
MAKEFLAGS += --silent --quiet
CXX      := -g++

#===============================================
CXXFLAGS := -w -MD -DWall 
CXXFLAGS += -std=c++0x -Wfatal-errors -Wno-deprecated-declarations
CXXFLAGS += -Wno-ignored-attributes -fopenmp  -Wno-register

#===============================================
BUILD    := ../build
INCLUDE  := ./include
OBJ_DIR  := $(BUILD)/objects
#===============================================
APP_DIR  := $(BUILD)/bin
TARGET   := cplex
#===============================================
#  CPLEX
#===============================================
CPXFLAGS := -DNDEBUG -DIL_STD 
#===============================================
# INCLUDES
#===============================================
INCLUDE  := -I${INCLUDE}
#===============================================
# LIBS
#===============================================
LibFLAGS += \
	-lm -lilocplex -lconcert -lcplex -lpthread -lcplexdistmip \
	-lbz2 -lblas -ldl -lz -llapack -lstdc++ 
#===============================================
# SOURCES
#===============================================
SRC      :=                      \
	$(wildcard src/*.cpp)         \
	$(wildcard src/*/*.cpp)         
#===============================================
# OBJECTS
#===============================================
OBJECTS := $(SRC:%.cpp=$(OBJ_DIR)/%.o)

#===============================================
# COMMANDS
#===============================================
all: build $(APP_DIR)/$(TARGET)

$(OBJ_DIR)/%.o: %.cpp
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(CPXFLAGS) $(INCLUDE) -o $@ -c $<

$(APP_DIR)/$(TARGET): $(OBJECTS)
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $(APP_DIR)/$(TARGET) $(OBJECTS) $(LibFLAGS)

.PHONY: all build clean debug release
.SILENT:clean


build:
	@mkdir -p $(APP_DIR)
	@mkdir -p $(OBJ_DIR)

debug: CXXFLAGS += -DDEBUG -g 
debug: all

release: CXXFLAGS += -march=native -O3 
release: all

clean:
	@rm -rf $(OBJ_DIR)/*
	@rm -rf $(APP_DIR)/*
	
-include $(OBJECTS:.o=.d)
